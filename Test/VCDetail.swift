//
//  VCDetail.swift
//  Test
//
//  Created by Сергей Гаврилов on 15/10/2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class VCDetail: UIViewController {
    
    // UI elements
    @IBOutlet weak var ivPoster: UIImageView!
    @IBOutlet weak var lTitle: UILabel!
    @IBOutlet weak var limdbRating: UILabel!
    @IBOutlet weak var lCountry: UILabel!
    @IBOutlet weak var lDirector: UILabel!
    @IBOutlet weak var lActors: UILabel!
    @IBOutlet weak var tvPlot: UITextView!
    
    // Id selected film
    var filmID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMore()
    }
    
    // Load and parse more info about selected film
    func loadMore() {
        ivPoster.kf.setImage(with: URL(string: "https://img.omdbapi.com/?apikey=4790e4e6&i=\(filmID)"))
        
        let urlString = "https://www.omdbapi.com/?apikey=4790e4e6&i=\(filmID)"
        let task = URLSession.shared.dataTask(with: URL(string: urlString)!) { (data, response, error) in
            if error != nil {
                print(error!)
            } else {
                do {
                    let json = try JSON(data: data!)
                    
                    DispatchQueue.main.async {
                        self.lTitle.text = json["Title"].stringValue
                        
                        self.limdbRating.attributedText = NSMutableAttributedString().bold("imdbRating:").normal(" \(json["imdbRating"].stringValue)")
                        self.lCountry.attributedText = NSMutableAttributedString().bold("Country:").normal(" \(json["Country"].stringValue)")
                        self.lDirector.attributedText = NSMutableAttributedString().bold("Director:").normal(" \(json["Director"].stringValue)")
                        self.lActors.attributedText = NSMutableAttributedString().bold("Actors:").normal(" \(json["Actors"].stringValue)")
                        self.tvPlot.text = json["Plot"].stringValue
                    }
                } catch {
                }
            }
        }
        task.resume()
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
