//
//  VCSearch
//  Test
//
//  Created by Сергей Гаврилов on 15/10/2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import SwiftyJSON

class VCSearch: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var filmsTitles: [String] = []
    var filmsID: [String] = []
    
    // UI elements
    @IBOutlet weak var lNoResults: UILabel!
    @IBOutlet weak var sbSearchBar: UISearchBar!
    @IBOutlet weak var tvSearchResult: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmsTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell")!
        cell.textLabel?.text = filmsTitles[indexPath.row]
        return cell
    }
    
    // Send selected filmId to detail View Controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            let destination: VCDetail = segue.destination as! VCDetail
            let indexPath = self.tvSearchResult.indexPathForSelectedRow
            destination.filmID = self.filmsID[indexPath!.row]
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Search and parse when taping
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let urlString = "https://www.omdbapi.com/?apikey=4790e4e6&s=\(searchBar.text!.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil))&type=movie"
        let task = URLSession.shared.dataTask(with: URL(string: urlString)!) { (data, response, error) in
            if error != nil {
                print(error!)
            } else {
                do {
                    let json = try JSON(data: data!)
                    print(json)
                    self.filmsTitles = json["Search"].arrayValue.map({$0["Title"].stringValue})
                    self.filmsID = json["Search"].arrayValue.map({$0["imdbID"].stringValue})
                    
                    DispatchQueue.main.async {
                        self.tvSearchResult.reloadData()
                        if 0 == self.filmsTitles.count {
                            self.lNoResults.isHidden = false
                        } else {
                            self.lNoResults.isHidden = true
                        }
                    }
                } catch {
                }
            }
        }
        task.resume()
    }
}

